#!/usr/bin/env python
# -*- coding: utf-8 -*-
from socket import socket, AF_INET, SOCK_DGRAM
import struct
import time

HOST = ''
PORT = 8890
DATA_INTERVAL_TIMEOUT=2 #sec

soc = socket(AF_INET, SOCK_DGRAM)
soc.settimeout(DATA_INTERVAL_TIMEOUT)
soc.bind((HOST, PORT))


while True:
    try:
        msg, address = soc.recvfrom(2048)
    except Exception as e:
        print("ERR!{}".format(e))
        time.sleep(0.5)
        continue
    pack=int(len(msg)/12)
    ar=[]
    for i in range(pack):
        step=i*12
        st=msg[step:step+12]
        #mv = memoryview(msg[i:12+(i*12)]).cast('iff')
        mv=struct.unpack('iff', st)
        ar.append(mv)
    print("rev: ", len(ar))
    for tp in ar:
        print("Q:{} theta:{} Dist:{}".format(*tp))


soc.close()