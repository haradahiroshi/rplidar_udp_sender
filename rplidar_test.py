from rplidar import RPLidar
import struct
from socket import socket, AF_INET, SOCK_DGRAM, timeout
import struct
import time

# lidar = RPLidar('/dev/ttyUSB0')
lidar = RPLidar('/dev/tty.SLAB_USBtoUART')
HOST = '127.0.0.1'
PORT = 8890

soc = socket(AF_INET, SOCK_DGRAM)
info = lidar.get_info()
print(info)
health = lidar.get_health()
print(health)

for i, scan in enumerate(lidar.iter_scans()):
    print('%d: Got %d measurments' % (i, len(scan)))
    bts=""
    for tp in scan:
        sb=struct.pack('iff', *tp)
        bts+=sb
    try:
        soc.sendto(bts, (HOST, PORT))
        print("send: ", len(bts))
    except Exception as e:
        print("ERR!{}".format(e))

    # if i > 10:
    #     break

lidar.stop()
lidar.stop_motor()
lidar.disconnect()