#RPLIDAR A2をUDPで飛ばすサンプル  
RPLIDAR A2の座標をUDPパケット経由で送受信するサンプル　

+ rplidar.py・・・・・python用ライブラリ  
+ rplidar_test.py・・送信用スクリプト
+ udpsv.py・・・・・・受信用スクリプト

##使用方法
+ 送信側PCにドライバーをインストール
+ 送信側PCにRPLIDARを接続
+ 送信側PC上でシリアルポートの名前を確認 ```ls -l /dev/tty.*```  
+ rplidar_test.pyの以下を設定
```
    lidar = RPLidar('/dev/tty.SLAB_USBtoUART')
    HOST = '127.0.0.1'
    PORT = 8890
```
+ 送信側PC上で起動```$python ./rplidar_test.py```
+ 受信側PC上で起動```$python ./udpsv.py```  

udpsv.pyのコンソールに座標情報が表示される
```
Q:15 theta:335.4375 Dist:2288.25
Q:15 theta:337.796875 Dist:2098.75
```

##RPLIDAR A2ライブラリ
ドライバー  
https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers  

python2.7用ライブラリ(メーカー公式は正常動作しない為、以下を使用)    
https://github.com/Roboticia/RPLidar

##参考　
RPLIDAR A2を動かすメモ　
https://qiita.com/nariakiiwatani/items/0386676dead09f9bbc7e　　